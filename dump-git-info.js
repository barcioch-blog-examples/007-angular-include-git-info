const git = require('git-rev-sync');
const { writeFileSync } = require('fs');

const version = {
  short: git.short(),
  long: git.long(),
  branch: git.branch(),
  date: git.date(),
};

writeFileSync('./src/assets/version.json', JSON.stringify(version));
